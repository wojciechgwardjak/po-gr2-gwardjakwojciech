package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw3.Arrays;
import java.util.Arrays;
import java.util.ArrayList;

public class cw3_arrays3 {

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        int i;
        int j;
        for (i = 0, j = 0; i < a.size(); i++) {
            for (int k = j; k < b.size(); k++) {
                if (a.get(i) >= b.get(k)) {
                    wynik.add(b.get(k));
                    j++;
                }
            }
            wynik.add(a.get(i));
        }
        for (; j < b.size(); j++)
            wynik.add(b.get(j));
        return wynik;
    }

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();

        a.add(5);
        a.add(4);
        a.add(8);
        a.add(10);
        a.add(3);
        a.add(6);
        b.add(20);
        b.add(30);
        b.add(40);
        b.add(50);
        b.add(60);
        b.add(70);
        System.out.println(Arrays.toString(mergeSorted(a, b).toArray()));
    }
}
