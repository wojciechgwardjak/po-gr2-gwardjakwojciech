package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw10;

public class PairUtil {
    public static <T> Pair<T> swap(Pair<T> para){
        return new Pair(para.getSecond(), para.getFirst());
    }
}
