package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw3;


public class cw3_zad23 {
    public static int ileZnakow(String str, char znak){
        if(str.indexOf(Character.toString(znak)) == -1) {
            return 0;
        }
        else{
            return 1 + ileZnakow(str.replaceFirst(Character.toString(znak), ""), znak);
        }
    }
    public static int ileSubow(String str, String sub){
        if(str.indexOf(sub) == -1){
            return 0;
        }
        else{
            return 1 + ileSubow(str.replaceFirst(sub, ""), sub);
        }
    }

    public static void main(String[] args){
        System.out.println(ileZnakow("C:\\Users\\Wojtek\\IdeaProjects\\javalekcje\\src\\pl\\edu\\uwm\\wmii\\gwardjakwojciech\\laboratorium00\\cw0\\cw3\\text.txt", 'h'));
        System.out.println(ileSubow("C:\\Users\\Wojtek\\IdeaProjects\\javalekcje\\src\\pl\\edu\\uwm\\wmii\\gwardjakwojciech\\laboratorium00\\cw0\\cw3\\text.txt", "hej"));
    }
}

