package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw2;
import java.util.Scanner;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;

public class zad2 {
     public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for(int i = 0; i < n; i++){
            tab[i] = r.nextInt(maxWartosc - minWartosc + 1) - maxWartosc;
        }
    }

    private static void wypisz(int[] tab){
        for(int el:tab){
            System.out.println(el + "");
        }
        System.out.println("");
    }

    public static int ileNieparzystych(int tab[]){
        int nieparzyste = 0;
        for(Integer i:tab){
        if(i%2!=0){
            nieparzyste += 1;
        }}return nieparzyste;}

    public static int ileParzystych(int tab[]){
            int parzyste = 0;
            for(Integer i:tab){
                if(i%2==0){
                    parzyste += 1;
                }
    }return parzyste;}

    public static int ileDodatnich(int tab[]){
                int dodatnie = 0;
                for(Integer i:tab){
                    if(i>0){
                        dodatnie += 1;
                    }}return dodatnie;}

    public static int ileUjemnych(int tab[]){
        int ujemne = 0;
        for(Integer i:tab){
            if(i<0){
                ujemne += 1;
            } }return ujemne; }

    public static int ileZerowych(int tab[]){
        int zerowe = 0;
        for(Integer i:tab){
            if(i>0){
                zerowe += 1; }
        }return zerowe; }

    public static int ileMaksymalnych(int tab[]){
        int maksymalny = tab[0];
        for(Integer i:tab){
            if(i>maksymalny){
                maksymalny = i;
            }
        }return maksymalny;
    }
    public static int sumaDodatnich(int tab[]){
        int sumaDodatnich = 0;
        for(Integer i:tab){
            if(i>0){
                sumaDodatnich += i; }
        }return sumaDodatnich;
    }
    public static int sumaUjemnych(int tab[]){
        int sumaUjemnych = 0;
        for(Integer i:tab){
            if(i<0){
                sumaUjemnych += i; }
        }return sumaUjemnych;
    }
    public static int dlugoscMaksCiagu(int tab[]){
        int ciagDodatnich = 0;
        int najwiekszyCiag = 0;
         for(Integer i:tab){
            if(i>0){
            ciagDodatnich += 1;
        }
        if(i<0){
            if(najwiekszyCiag < ciagDodatnich){
                najwiekszyCiag = ciagDodatnich;
            }
            ciagDodatnich = 0;
        }

    }return najwiekszyCiag;
     }
    public static void singum(int tab[]){
        for(int i=0; i<tab.length; i++){
            if(tab[i]>0){
                tab[i] = 1;
            }
        }
        for(int i=0; i<tab.length; i++){
            if(tab[i]<0){
                tab[i] = -1;
            }
        }
        for(long el:tab){
            System.out.println(el + "");
        }
    }
//    public static void odwrocFragment(int tab[], int lewy, int prawy){
//            int temp = 0;
//            for(int i=lewy; i<prawy; i++) {
//                if(lewy!=prawy){
//                    temp = tab[i];
//                    tab[i]=tab[prawy];
//                    tab[prawy]=temp;
//                    prawy--;
//                }
//            }
//            for(long el:tab){
//                System.out.println(el + "");
//            }
//    }

        public static void signum(int tab[]){
            for(int i=0; i<tab.length; i++){
                if(tab[i]>0){
                    tab[i] = 1;
                }
            }
            for(int i=0; i<tab.length; i++){
                if(tab[i]<0){
                    tab[i] = -1;
                }
            }
            for(long el:tab){
                System.out.println(el + "");
            }
        }
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe poczatkowych wyrazow tablicy: \n");
        int a = in.nextInt();
        int[] liczby = new int[a];
        generuj(liczby, a, -999, 999);

        System.out.println("Twoja tablica ma tyle elementów \n");
        wypisz(liczby);
        System.out.println("Twoja tablica ma " + ileParzystych(liczby) +" liczb parzystych\n");
        System.out.println("Twoja tablica ma " + ileNieparzystych(liczby) +" liczb nieparzystych\n");
        System.out.println("Twoja tablica ma " + ileUjemnych(liczby) +" liczb ujemnych\n");
        System.out.println("Twoja tablica ma " + ileDodatnich(liczby) +" liczb dodatnich\n");
        System.out.println("Twoja tablica ma " + ileZerowych(liczby) +" zer\n");
        System.out.println("Suma elementow ujemnych w Twojej tablicy jest rowna: " + sumaUjemnych(liczby) +" \n");
        System.out.println("Suma elementow dodatnich w Twojej tablicy jest rowna: " + sumaDodatnich(liczby) +" \n");
        System.out.println("Najdluzszy ciag liczb dodatnich w Twojej tablicy rowna sie: " + dlugoscMaksCiagu(liczby) +" \n");
    }
}
