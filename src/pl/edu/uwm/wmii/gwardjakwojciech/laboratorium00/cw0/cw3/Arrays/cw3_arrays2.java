package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw3.Arrays;
import java.util.ArrayList;
import java.util.Arrays;

public class cw3_arrays2 {
    public static ArrayList<Integer> merge(ArrayList<Integer>a, ArrayList<Integer> b){
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        int wiekszy;
        int mniejszy;
        if(a.size() > b.size()){
            wiekszy = a.size();
        }else{
            wiekszy = b.size();
        }
        if(wiekszy == a.size()){
            mniejszy = b.size();
        }else{
            mniejszy = a.size();
        }
        boolean aWiekszy = a.size() > b.size() ? true : false;


        int i;
        for (i = 0; i < mniejszy; i++) {
            wynik.add(a.get(i));
            wynik.add(b.get(i));
        }
        for (; i < wiekszy; i++) {
            if (aWiekszy)
                wynik.add(a.get(i));
            else
                wynik.add(b.get(i));
        }
        return wynik;

    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();
        a.add(5);
        a.add(4);
        a.add(8);
        a.add(10);
        a.add(3);
        a.add(6);
        b.add(20);
        b.add(30);
        b.add(40);
        b.add(50);
        b.add(60);
        b.add(70);
        System.out.println(Arrays.toString(merge(a, b).toArray()));
    }
}
