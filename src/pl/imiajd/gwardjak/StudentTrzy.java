package pl.imiajd.gwardjak;

import java.time.LocalDate;

public class StudentTrzy extends OsobaTrzy implements Cloneable, Comparable<OsobaTrzy>{
    public StudentTrzy(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen){
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }
    public double getSredniaOcen(){
        return this.sredniaOcen;
    }
    public int compareTo(StudentTrzy obj){
        int res = super.compareTo(obj);
        return res != 0? res : new Double(this.getSredniaOcen()).compareTo(new Double(obj.getSredniaOcen()));
    }

    public String toString(){
        return String.format("%s [%s %s] %.2f", this.getClass().getSimpleName(), this.getNazwisko(), this.getDataUrodzenia(), this.getSredniaOcen());
    }
    private double sredniaOcen;
}
