package pl.imiajd.gwardjak;

import java.time.LocalDate;

public class OsobaTrzy implements Comparable<OsobaTrzy>, Cloneable{
    public OsobaTrzy(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }
    public String getNazwisko(){
        return this.nazwisko;
    }
    public LocalDate getDataUrodzenia(){
        return this.dataUrodzenia;
    }

    @Override
    public String toString(){
        return String.format("%s [%s %s]", this.getClass().getSimpleName(), this.getNazwisko(), this.getDataUrodzenia() );
    }
    @Override
    public boolean equals(Object obj){
        if(this == obj) return true;
        if(obj == null || getClass() != obj.getClass()) return false;
        OsobaTrzy osoba = (OsobaTrzy) obj;
        return this.getNazwisko() == osoba.getNazwisko() && this.getDataUrodzenia() == osoba.getDataUrodzenia();
    }
    @Override
    public int compareTo(OsobaTrzy obj){
        int res = this.getNazwisko().compareTo(obj.getNazwisko());
        return res != 0 ? res : this.getDataUrodzenia().compareTo(obj.getDataUrodzenia());
    }
    private String nazwisko;
    private LocalDate dataUrodzenia;
}
