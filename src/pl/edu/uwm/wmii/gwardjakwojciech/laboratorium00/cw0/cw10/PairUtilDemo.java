package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw10;

public class PairUtilDemo {
    public static void main(String[] args){
        Pair<String> para = new Pair<>("Kantor", "Euro");
        Pair<String> swapped = PairUtil.swap(para);
        System.out.println(swapped.getFirst() + " " + swapped.getSecond());
    }
}
