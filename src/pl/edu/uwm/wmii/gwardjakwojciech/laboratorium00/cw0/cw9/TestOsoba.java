package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw9;

import pl.imiajd.gwardjak.OsobaTrzy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class TestOsoba {
    public static void main(String args[]){
        ArrayList<OsobaTrzy> grupa = new ArrayList<OsobaTrzy>();
        grupa.add(new OsobaTrzy("Kowalski", LocalDate.parse("1950-05-05")));
        grupa.add(new OsobaTrzy("Kowalski", LocalDate.parse("1958-05-05")));
        grupa.add(new OsobaTrzy("Nowakowsy", LocalDate.parse("1980-05-05")));
        grupa.add(new OsobaTrzy("Kawka", LocalDate.parse("1980-05-05")));
        grupa.add(new OsobaTrzy("Kolonsky", LocalDate.parse("1955-05-05")));
        System.out.println(grupa.toString());
//        grupa.sort(OsobaTrzy::compareTo);
        Collections.sort(grupa);
//        Arrays.sort(grupa); //nie działa
        System.out.println(grupa.toString());
    }
}
