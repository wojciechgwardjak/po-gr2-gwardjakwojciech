
package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw3;
import java.util.Scanner;
import java.lang.Object;
import java.lang.String;
import java.util.Arrays;

class cw3_zad1 {
    public static int countChar(String str, char c){
        int ile=0;
        for(int i = 0; i<str.length(); i++){
          if(str.charAt(i)==c){
            ile++;
          }
        }
        return ile;
    }

public static int countSubStr(String str, String subStr){
        int ile=0;
        for(int i=0; i<str.length()-subStr.length()+1; i++){
          String badana = str.substring(i, i + subStr.length());
          if(subStr.equals(badana)){
            ile++;
          }
          }

        return ile;
    }
public static String middle(String str){
    String srodkowa = "";
    if((str.length()%2) == 0){
        String litera = Character.toString(str.charAt(str.length()/2));
        String literaD = Character.toString(str.charAt(str.length()/2 - 1));
        srodkowa = srodkowa.concat(litera);
        srodkowa = srodkowa.concat(literaD);
    }else{
        double value = str.length()/2;
        int indeksLitery = (int)(value + 0.5);
        String literka = Character.toString(str.charAt(indeksLitery));
        srodkowa = srodkowa.concat(literka);
    }

    return srodkowa;
}

public static String repeat(String slowo, int ileRazy){
        String slowko = slowo;
        for(int i = 0; i < ileRazy; i++){
            slowo = slowo.concat(slowko);
        }
        return slowo;
}

public static int[] where(String str, String subStr){
        int[] where =  new int[50];
        int j = 0;
        int i = str.indexOf(subStr);
    while(i>=0){
        System.out.println(i);
        i = str.indexOf(subStr, i+1);
        where[j] = i;
        j++;
    }
    return where;

}
public static String change(String str){
    StringBuffer strB = new StringBuffer(str);
    for(int i =0; i<str.length(); i++){
        char c = strB.charAt(i);
        if(Character.isLowerCase(c)){
            strB.setCharAt(i, Character.toUpperCase(c));
        }else{ strB.setCharAt(i, Character.toLowerCase(c));}
    }
    str = strB.toString();
    return str;
}

public static String nice(String str){
    StringBuffer odwrocona = new StringBuffer(str);
    StringBuffer wynik = new StringBuffer();
    odwrocona.reverse();
    for(int i = 1, k = 0; k<odwrocona.length(); i++ ){
        if(i % 4 == 0){
            wynik.append('"');
        }else{
            wynik.append(odwrocona.charAt(k));
            k++;
        }
    }
    wynik.reverse();
    return wynik.toString();
}
public static String niceDruga(String str, char sep, int poz){
        StringBuffer odwrocona = new StringBuffer(str);
        StringBuffer wynik = new StringBuffer();
        odwrocona.reverse();
        for(int i = 1, k = 0; k<odwrocona.length(); i++ ){
            if(i % (poz + 1) == 0){
                wynik.append(sep);
            }else{
                wynik.append(odwrocona.charAt(k));
                k++;
            }
        }
        wynik.reverse();
        return wynik.toString();
    }

  public static void main(String[] args){
    Scanner napis = new Scanner(System.in);
    Scanner literkka = new Scanner(System.in);
    Scanner stringDlugi = new Scanner(System.in);
    Scanner substring = new Scanner(System.in);
    Scanner srodkowaLitera = new Scanner(System.in);
    Scanner konkatek = new Scanner(System.in);
    Scanner ile = new Scanner(System.in);
    Scanner glowny = new Scanner(System.in);
    Scanner podnapis = new Scanner(System.in);
    Scanner zamianka = new Scanner(System.in);
    Scanner odwracajka = new Scanner(System.in);
    Scanner znak = new Scanner(System.in);
    Scanner odstepny = new Scanner(System.in);
    Scanner mojznak = new Scanner(System.in);
    // System.out.println("Podaj napis do przeanalizowania pod katem ilosc znakow: \n");
    // String name = napis.nextLine();
    // System.out.println("Podaj znak, ktorego wystapienia chcesz policzyc: \n");
    // char c = literka.next().charAt(0);
    // System.out.println("Liczba wystapien Twojego znaku w napisie " + name + " rowna sie " + countChar(name,c) + ".\n");
    
    // System.out.println("Podaj napis do przeanalizowania pod katem ilosc zawierajacych substringow: \n");
    // String napistring = stringDlugi.nextLine();
    // System.out.println("Podaj substringa: \n");
    // String podstring = substring.nextLine();
    
    // System.out.println("Liczba wystapien Twojego substringa w napisie " + napistring + " rowna sie " + countSubStr(napistring, podstring) + ".\n");

//    System.out.println("Podaj napis, ktorego srodkowe litery chcesz uzyskac: \n");
//    String slowo = srodkowaLitera.nextLine();
//    System.out.println("Srodkowe litery Twojego wyrazu to: "+ middle(slowo)+"\n");

//      System.out.println("Podaj napis, ktory chcesz powtarzac: \n");
//      String konkat = konkatek.nextLine();
//      System.out.println("Ile slow chcesz dodac?: \n");
//      int ileRazy  = ile.nextInt();
//      System.out.println("Slowo po konkatenacji: "+ repeat(konkat, ileRazy) +"\n");

//      System.out.println("Podaj napis: \n");
//      String glownyNapis = glowny.nextLine();
//      System.out.println("Podaj podnapis: \n");
//      String podNapis = podnapis.nextLine();
//      System.out.println("Kolejne wystapienia Twojego podnapisu maja miejsce w: "+ where(glownyNapis, podNapis) +" indeksach tablicy\n");

//      System.out.println("Podaj napis ktorego wielkosc liter chcesz zamienic: \n");
//      String zmiennicy = zamianka.nextLine();
//      System.out.println("Twoje slowo po zamianie wyglada tak: " + change(zmiennicy) +"\n");


//      System.out.println("Podaj napis do ktorego chcesz wstawic apostrof: \n");
//      String odwroc = odwracajka.nextLine();
//      System.out.println("Twoj napis z apostrofami to: "+ nice(odwroc)+"\n");

      System.out.println("Podaj napis do ktorego chcesz wstawic swoj znak: \n");
      String znakNapis = znak.nextLine();
      System.out.println("Podaj odstep: \n");
      int odstep = odstepny.nextInt();
      System.out.println("Podaj znak: \n");
      char znacznik = mojznak.next().charAt(0);
      System.out.println("Twoj napis z Twoimi znakiem to: "+ niceDruga(znakNapis, znacznik, odstep)+"\n");

  }
}