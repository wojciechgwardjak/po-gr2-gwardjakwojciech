package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw3.Arrays;
import java.util.Arrays;
import java.util.ArrayList;

public class cw3_arrays4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        for (int i = a.size() - 1; i >= 0; i--)
            wynik.add(a.get(i));
        return wynik;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(5);
        a.add(6);
        a.add(7);
        System.out.println(Arrays.toString(reversed(a).toArray()));
    }
}
