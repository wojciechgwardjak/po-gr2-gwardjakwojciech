package pl.imiajd.gwardjak;

public class Nauczyciel extends Osoba {
    public Nauczyciel(String nazwisko, int rokUrodzenia, double pensja){
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }
    public double getPensja(){
        return this.pensja;
    }
    @Override
    public String toString(){
        return String.format("%s, %.2f", super.toString(), this.pensja);
    }
    private double pensja;
}
