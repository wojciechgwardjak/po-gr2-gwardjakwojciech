package pl.imiajd.gwardjak;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable, Comparable<Komputer> {
    public Laptop(String nazwa, LocalDate dataProdukcji, boolean czyApple){
        super(nazwa, dataProdukcji);
        this.czyApple = czyApple;
    }
    public boolean getCzyApple(){
        return this.czyApple;
    }
    public String toString(){
        return String.format("[%s %s %s]", this.getNazwa(), this.getDataProdukcji(), this.getCzyApple()? "apple" : "nie-apple");
    }
    public int compareTo(Laptop obj){
        int result = super.compareTo(obj);
        return result != 0 ? result : new Boolean(this.czyApple).compareTo(new Boolean(obj.czyApple));
    }
    private boolean czyApple;
}
