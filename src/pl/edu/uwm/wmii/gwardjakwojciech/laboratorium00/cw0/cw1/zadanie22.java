package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw1;
import java.util.Scanner;
import java.lang.Math;
import java.util.ArrayList;

public class zadanie22 {
 public static void main(String[] args) {
     Scanner in = new Scanner(System.in);
        System.out.println("Podaj ilosc liczb rzeczywistych jakie chcesz zaladowac: \n");
    int a = in.nextInt();

    ArrayList<Integer> liczbyNaturalne = new ArrayList<Integer>(a);

    int kolejnaLiczba;
    for(int i=1; i<=a; i++){
      System.out.println("Podaj kolejna liczbe rzeczywista \n");
      kolejnaLiczba = in.nextInt();
      liczbyNaturalne.add(kolejnaLiczba);
    }
    for(Integer i:liczbyNaturalne){
      System.out.println(i + " ");
    }

    int sumaDodatnich = 0;
    for(Integer i:liczbyNaturalne){
      if(i>0){
        sumaDodatnich+=i;
      }
    }
     System.out.println("Suma dodatnich liczb rzeczywistych Twojej listy rowna sie: \n"+ sumaDodatnich);

  }

}
