package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ArrayUtil {
    public static <T extends Comparable<T>> boolean isSorted(ArrayList<T>tab){
        ArrayList<T> lista = (ArrayList) tab.clone();
        tab.sort(new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.compareTo(o2);
            }
        });
        return tab.equals(lista);
    }

    public static <T extends Comparable<T>> int binSearch(ArrayList<T> tab, T tabSecond){
        int k = tab.size() - 1;
        int pivot = k/2;
        int i = 0;
        while(i!=k){
            if(tab.get(pivot).compareTo(tabSecond)==-1) i = pivot;
            else if (tab.get(pivot).compareTo(tabSecond)==1) k = pivot;
            else return pivot;
            pivot = (k + 1)/2;
        }
        if(tab.get(i).equals(tabSecond)) return 1;
        else return -1;
    }

    public static <T extends Comparable<T>> void selectionSort(ArrayList<T> tab){
        T min;
        for(int i = 0; i<tab.size(); i++){
            min = Collections.min(tab.subList(i, tab.size()));
            for(int k = i + 1; k<tab.size();k++){
                if(tab.get(k).equals(min)){
                    Collections.swap(tab, i, k);
                }
            }
        }
    }

}
