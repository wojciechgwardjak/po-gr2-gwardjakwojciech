package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw4;


public class RachunekBankowy{
    public RachunekBankowy(double saldo){
        this.saldo = saldo;
    }

    public double obliczMiesieczneOdsetki(){
        return (saldo * rocznaStopaProcentowa) / 12;
    }

    public void dodajDoSalda(){
        saldo += obliczMiesieczneOdsetki();
    }
    public double stanKonta(){
        return saldo;
    }
    public static void setRocznaStopaProcentowa(double wartosc){
        rocznaStopaProcentowa = wartosc;
    }

    private double saldo;
    public static double rocznaStopaProcentowa;

}