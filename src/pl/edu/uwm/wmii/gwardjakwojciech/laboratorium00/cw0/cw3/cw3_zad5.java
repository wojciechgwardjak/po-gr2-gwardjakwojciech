package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw3;
import java.math.BigDecimal;
import java.util.Scanner;

public class cw3_zad5 {
    public static BigDecimal suma(BigDecimal kapital, int p, int n){
        BigDecimal suma = kapital;
        for(int i = 0; i < n; i++){
            suma = suma.multiply(BigDecimal.ONE.add(BigDecimal.valueOf(p).divide(BigDecimal.valueOf(100))));

        }
        return suma;
    }
    public static void main(String[] args){
        Scanner kapital = new Scanner(System.in);
        Scanner p = new Scanner(System.in);
        Scanner n = new Scanner(System.in);
        System.out.println("Podaj Twoj kapital poczatkowy \n" );
        int kapitalny = kapital.nextInt();
        System.out.println("Podaj p \n" );
        int pe = p.nextInt();
        System.out.println("Podaj n \n" );
        int en = n.nextInt();
        System.out.println("Suma Twojego kapitalu rowna sie:" + suma(BigDecimal.valueOf(kapitalny), pe, en) );
    }
}
