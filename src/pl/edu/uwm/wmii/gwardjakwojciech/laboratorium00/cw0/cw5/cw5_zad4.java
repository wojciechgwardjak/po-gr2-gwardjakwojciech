package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw5;
import pl.imiajd.gwardjak.Nauczyciel;
import pl.imiajd.gwardjak.Student;

public class cw5_zad4 {
    public static void main(String[] args){
        Nauczyciel nauczyciel = new Nauczyciel("SnapeSewerus", 1950, 5555.55);
        Student student = new Student("WelseyRon", 1990, "eliksiry");
        System.out.println(student.getKierunek());
        System.out.println(nauczyciel.getPensja());
        System.out.println(nauczyciel.toString());
        System.out.println(student.toString());
    }
}
