package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw2;
import java.util.Scanner;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;

public class zad1 {
public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe poczatkowych wyrazow tablicy: \n");
        int a = in.nextInt();
      
        int min = -999;
        int max = 999;
        int parzyste = 0;
        int nieparzyste = 0;
        int ujemne = 0;
        int dodatnie = 0;
        int zero = 0;
        int sumaUjemnych = 0;
        int sumaDodatnich = 0;
        int ciagDodatnich = 0;
        int najwiekszyCiag = 0;
        int[]liczby = new int[a];
     
        int ileMax = 0;
        Random generator = new Random();

        for(int i=0; i<liczby.length; i++){
            liczby[i] = (generator.nextInt(max - min + 1) - max);
        }
        int maksymalnyElement = liczby[0];
        System.out.println("------------------ \n");
        // System.out.println("Twoja lista sklada sie z liczb\n");
         System.out.println("Twoja lista sklada sie z liczb: " + Arrays.toString(liczby) + "\n");
        // for(Integer i:liczby){
        //   System.out.println(i + " ");
        // }
        for(Integer i:liczby){
          if(i%2==0){
            parzyste += 1;
          }
          if(i%2!=0){
            nieparzyste += 1;
          }
          if(i<0){
            ujemne += 1;
            sumaUjemnych += i;
          }
          if(i>0){
            dodatnie += 1;
            sumaDodatnich += i;
          }
          if(i==0){
            zero += 1;
          }
          if(i>maksymalnyElement){
            maksymalnyElement = i;
          }
         }
         for(Integer i:liczby){
         if(maksymalnyElement == i){
            ileMax += 1;
          }}

          for(Integer i:liczby){ 
           if(i>0){
            ciagDodatnich += 1;
         }
         if(i<0){
         if(najwiekszyCiag < ciagDodatnich){
          najwiekszyCiag = ciagDodatnich;
          }
          ciagDodatnich = 0;
          }}
           

        System.out.println("Twoja tablica ma " + parzyste +" liczb parzystych\n");
        System.out.println("Twoja tablica ma " + nieparzyste +" liczb nieparzystych\n");
        System.out.println("Twoja tablica ma " + ujemne +" liczb ujemnych\n");
        System.out.println("Twoja tablica ma " + dodatnie +" liczb dodatnich\n");
        System.out.println("Twoja tablica ma " + zero +" zer\n");
        System.out.println("Element maksymalny Twojej tablicy to: " + maksymalnyElement +", wystepuje on "+ ileMax +" razy w Twojej tablicy \n");
        System.out.println("Twoja tablica ma " + dodatnie +" liczb dodatnich\n");
        System.out.println("Suma elementow ujemnych w Twojej tablicy jest rowna: " + sumaUjemnych +" \n");
        System.out.println("Suma elementow ujemnych w Twojej tablicy jest rowna: " + sumaDodatnich +" \n");
        System.out.println("Najdluzszy ciag liczb dodatnich w Twojej tablicy rowna sie: " + najwiekszyCiag +" \n");


        System.out.println("Podaj lewa granice tablicy do odwrocenia: \n");
        int lewy = in.nextInt();
        System.out.println("Podaj prawa granice tablicy do odwrocenia: \n");
        int prawy = in.nextInt();
         int temp;

         for(int i=lewy; i<prawy; i++) {
            if(lewy!=prawy){
            temp = liczby[i];
            liczby[i]=liczby[prawy];
                liczby[prawy]=temp;
            prawy--;
            }
        }
        System.out.println("Odwrocona kolejnosc tablicy: "+ Arrays.toString(liczby) + "\n");

    for(int i=0; i<a; i++){
        if(liczby[i]>0){
            liczby[i] = 1;
        }
    }
    for(int i=0; i<a; i++){
        if(liczby[i]<0){
            liczby[i] = -1;
        }
    }
    System.out.println("Twoja nowa lista sklada sie z liczb: " + Arrays.toString(liczby)+ "\n");


}
}
