package pl.imiajd.gwardjak;

public class Osoba {
    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }
    public String getNazwisko(){
        return this.nazwisko;
    }
    public int getRokUrodzenia(){
        return this.rokUrodzenia;
    }
    @Override
    public String toString(){
        return String.format("%d, %s", this.rokUrodzenia, this.nazwisko);
    }

    private String nazwisko;
    private int rokUrodzenia;
}
