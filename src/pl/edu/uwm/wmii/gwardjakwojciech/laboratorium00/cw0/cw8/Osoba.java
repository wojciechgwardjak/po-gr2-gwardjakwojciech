package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw8;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
public class Osoba implements  Comparable{
    public Osoba(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    @Override
    public String toString(){
        return String.format("%s, %s data urodzenia: %s", this.getClass().getSimpleName(), this.nazwisko, this.dataUrodzenia );
    }
    @Override
    public boolean equals(Object next){
        if (this == next)
            return true;
        if(next == null)
            return false;
        if(!(next instanceof Osoba))
            return false;
        Osoba nextObject = (Osoba) next;

        return this.nazwisko == nextObject.nazwisko && this.dataUrodzenia == nextObject.dataUrodzenia;
    }
    @Override
    public int compareTo(Object next){
        Osoba nextObject = (Osoba) next;
        int cNazwisko = this.nazwisko.compareTo(nextObject.nazwisko);
        int cdataUrodzenia = this.dataUrodzenia.compareTo(nextObject.dataUrodzenia);
        return cNazwisko != 0 ? cNazwisko : cdataUrodzenia;
    }
    public int ileLat(){
        return Period.between(this.dataUrodzenia, LocalDate.now()).getYears();
    }
    public int ileMiesiecy(){
        return this.dataUrodzenia.getMonthValue() > LocalDate.now().getMonthValue()? LocalDate.now().getMonthValue() + (12-this.dataUrodzenia.getMonthValue()) : (LocalDate.now().getMonthValue() - this.dataUrodzenia.getMonthValue());
    }
    public int ileDni(){

        return Math.abs((int)ChronoUnit.DAYS.between(LocalDate.now(), this.dataUrodzenia.withYear(LocalDate.now().getYear()).withMonth(LocalDate.now().getMonthValue())));
    }
    private LocalDate dataUrodzenia;
    private String nazwisko;
}
