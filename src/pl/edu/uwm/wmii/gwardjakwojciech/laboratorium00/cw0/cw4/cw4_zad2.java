package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw4;

public class cw4_zad2 {
    public static void main(String[] args){
        IntegerSet nowy = new IntegerSet();
        IntegerSet nowyNowy = new IntegerSet();
        nowy.insertElement(8);
        nowy.insertElement(9);
        nowy.deletElement(8);
        nowy.insertElement(55);
        nowyNowy.insertElement(5);
        nowyNowy.insertElement(5);
        nowyNowy.insertElement(5);
        System.out.println(String.format("Nowy IntegerSet zawiera: %s", nowy.toString()));
        System.out.println(String.format("Czy nowy równa się nowyNowy?: %b", nowy.equals(nowyNowy)));

    }
}
