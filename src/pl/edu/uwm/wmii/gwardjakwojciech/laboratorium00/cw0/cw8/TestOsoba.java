package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw8;

import java.time.LocalDate;
import java.util.Arrays;

public class TestOsoba {
    public static void main(String[] args){
        Osoba[] grupa = new Osoba[5];
        grupa[0] = new Osoba("Nowakowski", LocalDate.parse("1990-05-05"));
        grupa[1] = new Osoba("Nowakowski", LocalDate.parse("1996-06-08"));
        grupa[2] = new Osoba("Kaznodzieja", LocalDate.parse("2000-08-08"));
        grupa[3] = new Osoba("Ktos", LocalDate.parse("2005-05-06"));
        grupa[4] = new Osoba("Czagry", LocalDate.parse("2010-08-05"));
        System.out.println(Arrays.toString(grupa));
        Arrays.sort(grupa);
        System.out.println(Arrays.toString(grupa));
        for(Osoba osoba : grupa){
            System.out.println(String.format("Lat: %d, miesięcy: %d, dni: %d", osoba.ileLat(), osoba.ileMiesiecy(), osoba.ileDni()));
        }
    }
}
