package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw7;

import pl.imiajd.gwardjak.Fortepian;
import pl.imiajd.gwardjak.Flet;
import pl.imiajd.gwardjak.Instrument;
import pl.imiajd.gwardjak.Skrzypce;

import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalTime;


public class TestInstrumenty {
    public static void main(String[] args){
        ArrayList<Instrument> instrumenty = new ArrayList<>();
        instrumenty.add(new Skrzypce("Stradivarius", LocalDate.of(1800,5,5)));
        instrumenty.add(new Fortepian("Yamaha", LocalDate.of(1850, 5, 5)));
        instrumenty.add(new Flet("Kawasaki", LocalDate.of(2005,5,5)));
        instrumenty.add(new Fortepian("Stradivarius", LocalDate.of(1900,5,5)));
        instrumenty.add(new Flet("Kawasaki", LocalDate.of(1950,5,5)));
        for(Instrument inst : instrumenty){
            System.out.println(inst.dzwiek());
        }
        System.out.println(instrumenty.toString());
    }
}




