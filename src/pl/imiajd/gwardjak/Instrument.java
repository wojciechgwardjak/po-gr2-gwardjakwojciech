package pl.imiajd.gwardjak;

import java.time.LocalDate;
import java.util.Objects;

public abstract class Instrument {
    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }
    public Instrument(){
        this.producent = "Yamaha";
        this.rokProdukcji = LocalDate.of(1999, 05,05);
    }
    public String getProducent(){
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }
    public abstract String dzwiek();
    @Override
    public boolean equals(Object obj){
        if(this == obj) return true;
        if(obj == null || getClass() != obj.getClass()) return false;
        Instrument next = (Instrument) obj;
        return this.getProducent() == next.getProducent() && this.getRokProdukcji() == next.getRokProdukcji();
    }
    @Override
    public String toString(){
        return String.format("Instrument: %s, autor: %s, rok produkcji: %s", this.getClass().getSimpleName(), this.getProducent(), this.getRokProdukcji().getYear());
    }
    private String producent;
    private LocalDate rokProdukcji;
}
