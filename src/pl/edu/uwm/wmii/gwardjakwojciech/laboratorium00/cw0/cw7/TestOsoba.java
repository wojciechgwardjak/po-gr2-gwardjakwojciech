package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw7;
import java.time.LocalDate;
import pl.imiajd.gwardjak.OsobaDwa;
import pl.imiajd.gwardjak.PracownikDwa;
import pl.imiajd.gwardjak.StudentDwa;
public class TestOsoba {
    public static void main(String[] args){

        OsobaDwa[] ludzie = new OsobaDwa[3];
        ludzie[0] = new PracownikDwa("Kowalski", "Jan",true  , LocalDate.parse("1990-05-05"), LocalDate.parse("1998-05-05"), 5000);
        ludzie[1] = new StudentDwa("Nowakowska", "Małgorzata", false  ,LocalDate.parse("1990-05-05")  ,"kuglarstwo", 5.55);
        ludzie[2] = new StudentDwa("Zalodkowski", "Maciej", true, LocalDate.parse("1990-05-05"),"zoladkowo", 5.80 );

        for(OsobaDwa p : ludzie) {
//            System.out.println(p.getNazwisko() + ": " + p.getOpis());
            System.out.println(String.format("%s %s plec %b %s %s ", p.getNazwisko(), p.getImie(), p.getPlec(), p.getDataUrodzenia(), p.getOpis()));
        }
//        ludzie[2].setSredniaOcen(5);
//        System.out.println(String.format("%s %s plec %b %s %s"));
    }
}


