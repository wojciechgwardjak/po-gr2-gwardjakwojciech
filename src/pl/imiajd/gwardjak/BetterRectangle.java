package pl.imiajd.gwardjak;
import java.awt.Rectangle;

public class BetterRectangle extends Rectangle{
    public BetterRectangle(int x, int y, int height, int width ){
        super(x, y, height, width);
        super.setLocation(x, y);
        super.setSize(width, height);
    }
    public double getPerimeter(){
        return 2 * (this.width + this.height);
    } public double getArea(){
        return this.width * this.height;
    }
}
