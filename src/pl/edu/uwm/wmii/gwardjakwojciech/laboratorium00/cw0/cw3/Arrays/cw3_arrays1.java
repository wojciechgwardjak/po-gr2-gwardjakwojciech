package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw3.Arrays;
import java.util.ArrayList;
import java.util.Arrays;

public class cw3_arrays1 {
    public static ArrayList<Integer> append(ArrayList<Integer>a, ArrayList<Integer > b){
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        for(int i = 0; i < a.size(); i++)
            wynik.add(a.get(i));
        for(int i = 0; i < b.size(); i++)
            wynik.add(b.get(i));
        return wynik;
    }
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();
        a.add(5);
        a.add(5);
        a.add(5);
        b.add(15);
        b.add(15);
        b.add(15);

        System.out.println(Arrays.toString(append(a, b).toArray()));
    }
}
