package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw1;
import java.util.Scanner;
public class zadanie21 {
 public static void main(String[] args) {
   Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe do ktorej chcesz dodawac \n");
    int a = in.nextInt();
    int ileNieparzystych = 0;
    int podzielnePrzez3 = 0;
    int kwadraty = 0;
    int dziwnyWarunek = 0;
    int dziwneZalozenie = 0;
    int ileNieparzystychNieujemnych = 0;
    int dziwniejszyWarunek = 0;
  	 for(int i=1; i<=a;i++) {
       if((i%2)!=0 ){
         ileNieparzystych+=1;
       }
       if((i%3)==0 && (i%5!=0)){
         podzielnePrzez3 += 1;
       }
       if((Math.sqrt(i))%2==0){
         kwadraty += 1;
      }
       if(((Math.pow(2, i))<i) &&(i < (silnia(i)))){
         dziwneZalozenie += 1;
       }
       if((i%2!=0) && (i>0)){
        ileNieparzystychNieujemnych += 1;
       }
       if((Math.abs(i)) < (Math.pow(i, 2))){
         dziwniejszyWarunek += 1;
       }
      }
      for(int i=2; i<a;i++) {
        if(i < (((i-1) + (i+1))/2)){
          dziwnyWarunek += 1;
        }
      }
       
      System.out.println("W przedziale od 1 do "+ a +" jest tyle liczb nieparzystych: " + ileNieparzystych);
      System.out.println("W przedziale od 1 do "+ a +" jest tyle liczb podzielnych przez 3 i niepodzielnych przez 5: " + podzielnePrzez3);
      System.out.println("W przedziale od 1 do "+ a +" jest tyle liczb, ktore sa kwadratami liczby parzystej " + kwadraty);
      System.out.println("W przedziale od 1 do "+ a +" jest tyle liczb, ktore spelniaja dziwny warunek: " + dziwnyWarunek);
      System.out.println("W przedziale od 1 do "+ a +" jest tyle liczb, ktore spelniaja dziwne zalozenie " + dziwneZalozenie);
      System.out.println("W przedziale od 1 do "+ a +" nie ma liczb, które mają indeks nieparzysty oraz sa liczbami parzystymi");
      System.out.println("W przedziale od 1 do "+ a +" jest tyle liczb, ktore sa nieparzyste i nieujemne " + ileNieparzystychNieujemnych);
      System.out.println("W przedziale od 1 do "+ a +" jest tyle liczb, ktore spelniaja jeszcze dziwniejszy warunek " + dziwniejszyWarunek);
}
 public static int silnia(int i) 
  {
    if (i == 0) 
      return 1;
    else 
      return i * silnia(i - 1);
  }



}
