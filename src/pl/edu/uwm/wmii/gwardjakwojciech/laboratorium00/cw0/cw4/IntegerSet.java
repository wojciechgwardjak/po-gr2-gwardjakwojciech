package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw4;

import java.util.ArrayList;

public class IntegerSet{
    public IntegerSet(){
        this.table = new boolean[100];
        for(int i = 0; i < table.length; i++)
            this.table[i] = false;
    }
    public IntegerSet(Integer[] tableSet){
        for(int i = 0; i < table.length; i++)
            table[i] = false;
        for(int k : tableSet)
            this.table[k] = true;
    }
    public IntegerSet(boolean[] bool){
        for(int i = 0; i < 100; i++){
            this.table[i] = bool[i];
        }
    }
    public static IntegerSet union(IntegerSet data, IntegerSet data2){
        boolean[] wynik = new boolean[100];
        for(int i = 0; i < wynik.length; i++)
            wynik[i] = data.table[i] || data2.table[i];
        return new IntegerSet(wynik);
    }
    public static IntegerSet intersection(IntegerSet data, IntegerSet data2){
        boolean[] wynik = new boolean[100];
        for(int i = 0; i < 100; i++)
            wynik[i] = data.table[i] && data2.table[i];
        return new IntegerSet(wynik);
    }
    public void insertElement(int liczba){
        this.table[liczba] = true;
    }
    public void deletElement(int liczba){
        this.table[liczba] = false;
    }
    public String tooString(){
        StringBuffer wyniki = new StringBuffer("");
        for(int i = 0; i < 100; i++){
            if(this.table[i]){
                wyniki.append(i);
                wyniki.append(" ");
            }
        }
        return wyniki.toString();
    }
    @Override
    public boolean equals(Object objekt){
        if (this == objekt) return true;
        if (objekt == null) return false;
        if(!(objekt instanceof IntegerSet)) return false;
        IntegerSet nowy = (IntegerSet) objekt;

        for(int i = 0; i<100; i++){
            if(this.table[i] != nowy.table[i])
                return false;        }
        return true;
    }


    public boolean[] table;
}
