package pl.imiajd.gwardjak;

public class Adres {
    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }
    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = 0;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }
    public void pokaz(){
        System.out.println("Twoj kod pocztowy: " + this.kod_pocztowy + ", a miasto w którym mieszkasz: " + this.miasto +"\n");
        System.out.println("Twoj numer domu: " + this.numer_domu + ", a numer mieszkania: " + this.numer_mieszkania + ". Ulica na której mieszkasz to " + this.ulica + ". Numer Twojego mieszkania to: " + String.format("%d", this.numer_mieszkania == 0? "" : this.numer_mieszkania ));
    }
    public boolean przed(Adres adr){
       return Integer.parseInt(adr.kod_pocztowy) < Integer.parseInt(adr.kod_pocztowy);
    }

    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;
}
