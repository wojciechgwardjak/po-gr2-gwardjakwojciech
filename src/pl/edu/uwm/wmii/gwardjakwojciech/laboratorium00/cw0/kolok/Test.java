package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.kolok;

import pl.imiajd.gwardjak.Komputer;
import pl.imiajd.gwardjak.Laptop;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Test {
    public static void main (String args[]){
        ArrayList<Komputer>grupa = new ArrayList<Komputer>();
        grupa.add(new Komputer("stationary", LocalDate.parse("2005-05-05")));
        grupa.add(new Komputer("stationary", LocalDate.parse("2005-05-08")));
        grupa.add(new Komputer("pecet", LocalDate.parse("2008-05-05")));
        grupa.add(new Komputer("eskto", LocalDate.parse("2008-05-05")));
        grupa.add(new Komputer("centralny", LocalDate.parse("2015-05-05")));
        System.out.println(grupa.toString());
        Collections.sort(grupa);
        System.out.println(grupa.toString());
//        for(Komputer element : grupa){
//            System.out.println(element);
//        }
        ArrayList<Laptop>grupaLaptopow = new ArrayList<Laptop>();
        grupaLaptopow.add(new Laptop("macek", LocalDate.parse("2008-05-05"), true));
        grupaLaptopow.add(new Laptop("lestaro", LocalDate.parse("2008-05-05"), false));
        grupaLaptopow.add(new Laptop("hapek", LocalDate.parse("2008-05-05"), true));
        grupaLaptopow.add(new Laptop("macek", LocalDate.parse("2008-05-05"), false));
        grupaLaptopow.add(new Laptop("nsns", LocalDate.parse("2008-05-05"), true));
        System.out.println(grupaLaptopow.toString());
//        for(Laptop element : grupaLaptopow){
//            System.out.println(element);
//        }
        Collections.sort(grupaLaptopow);
        System.out.println(grupaLaptopow.toString());

//        for(Laptop element : grupaLaptopow){
//            System.out.println(element);
//        }
    }
}
