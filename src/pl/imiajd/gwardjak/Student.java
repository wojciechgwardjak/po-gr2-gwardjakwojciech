package pl.imiajd.gwardjak;

import java.time.LocalDate;

public class Student extends Osoba {
    public Student(String nazwisko, int rokUrodzenia, String kierunek){
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }
    public String getKierunek(){
        return this.kierunek;
    }
    @Override
    public String toString(){
        return String.format("%s, %s", super.toString(), this.kierunek);
    }

    private String kierunek;
}
