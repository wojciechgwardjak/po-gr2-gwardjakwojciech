package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw10;

public class Pair<T> {
    public Pair(){
        first = null;
        second = null;
    }
    public Pair(T first, T second){
        this.first = first;
        this.second = second;
    }
    public T getFirst() {return first;}
    public T getSecond(){return second;}
    public void setFirst(T other){ this.first = other;}
    public void setSecond(T other){this.second = other;}
    public void swap(){
        T temp = this.first;
        this.first = this.second;
        this.second = temp;
    }
    private T first;
    private T second;
}