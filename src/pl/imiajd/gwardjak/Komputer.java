package pl.imiajd.gwardjak;

import java.time.LocalDate;
import java.util.Objects;

public class Komputer implements Cloneable, Comparable<Komputer>{
    public Komputer(String nazwa, LocalDate dataProdukcji){
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
    }
    public String getNazwa(){
        return this.nazwa;
    }
    public LocalDate getDataProdukcji(){
        return this.dataProdukcji;
    }
    public String toString(){
        return String.format("[%s %s]", this.getNazwa(), this.getDataProdukcji());
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj) return false;
        if (obj == null || getClass() != obj.getClass()) return false;
        Komputer os = (Komputer) obj;
        return Objects.equals(nazwa, os.nazwa) && Objects.equals(dataProdukcji, os.dataProdukcji);
    }
    @Override
    public int compareTo(Komputer obj){
        int result = this.nazwa.compareTo(obj.nazwa);
        return result !=0 ? result : this.dataProdukcji.compareTo(obj.dataProdukcji);
    }
    @Override
    protected Object clone() throws CloneNotSupportedException{
        return super.clone();
    }
    private String nazwa;
    private LocalDate dataProdukcji;
}
