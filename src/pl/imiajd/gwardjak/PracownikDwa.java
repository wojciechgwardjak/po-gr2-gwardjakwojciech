package pl.imiajd.gwardjak;

import java.time.LocalDate;

public class PracownikDwa extends OsobaDwa{
    public PracownikDwa(String nazwisko, String imie, boolean plec, LocalDate dataUrodzenia, LocalDate dataZatrudnienia, double pobory){
        super(nazwisko, imie, plec, dataUrodzenia);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }
    public double getPobory(){
        return pobory;
    }
    public String getOpis(){
        return String.format("Pracownik z pensja %.2f zł", pobory);
    }
    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }
    private LocalDate dataZatrudnienia;
    private double pobory;
}
