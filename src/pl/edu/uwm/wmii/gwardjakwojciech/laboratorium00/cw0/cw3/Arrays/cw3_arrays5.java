package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw3.Arrays;
import java.util.Arrays;
import java.util.ArrayList;


public class cw3_arrays5 {
    public static void reverse(ArrayList<Integer> a) {
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        for (int i = a.size() - 1; i >= 0; i--)
            wynik.add(a.get(i));
        a.clear();
        for (int i = 0; i < wynik.size(); i++)
            a.add(wynik.get(i));
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(7);
        a.add(8);
        a.add(9);
        a.add(10);
        reverse(a);
        System.out.println(Arrays.toString(a.toArray()));
    }
}
