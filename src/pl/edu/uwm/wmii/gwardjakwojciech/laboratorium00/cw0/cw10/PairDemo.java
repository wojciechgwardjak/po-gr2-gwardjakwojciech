package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw10;

public class PairDemo {
    public static void main(String[] args){
        Pair<String> para = new Pair<>("Kantor", "Euro");
        para.swap();
        System.out.println(para.getFirst() + " " + para.getSecond());
    }
}
