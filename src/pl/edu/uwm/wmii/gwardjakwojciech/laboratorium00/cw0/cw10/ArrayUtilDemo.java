package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw10;

import java.time.LocalDate;
import java.util.ArrayList;

public class ArrayUtilDemo {
    public static void main(String[] args){
        ArrayList<Integer> tab = new ArrayList<>();
        tab.add(5);
        tab.add(15);
        tab.add(80);
        tab.add(50);
        System.out.println(String.format("Tablica posortowana? %b", ArrayUtil.isSorted(tab)));
        System.out.println(ArrayUtil.binSearch(tab, 5));
        ArrayUtil.selectionSort(tab);
        System.out.println(tab);
        ArrayList<Integer> tablica = new ArrayList<>();
        tablica.add(10);
        tablica.add(8);
        tablica.add(12);
        tablica.add(15);
        System.out.println(ArrayUtil.binSearch(tablica, 15));
        ArrayUtil.selectionSort(tablica);
        System.out.println(tablica);
        ArrayList<LocalDate> lista = new ArrayList<LocalDate>();
        lista.add(LocalDate.parse("2005-05-05"));
        lista.add(LocalDate.parse("2008-08-05"));
        lista.add(LocalDate.parse("2025-05-05"));
        lista.add(LocalDate.parse("2009-09-09"));
//        ArrayUtil.selectionSort(lista);
    }
}

