package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw3;
import java.math.BigInteger;
import java.util.Scanner;

public class cw3_zad4 {
    public static BigInteger szachy(int n){
        BigInteger wynik = BigInteger.ZERO;
        for (int i = 0; i < n; i++){
            wynik = wynik.add(BigInteger.valueOf(2).pow(i));
        }
        return wynik;
    }
    public static void main(String[] args){
        Scanner next = new Scanner(System.in);
        System.out.println("Podaj liczbe kolumn Twojej szachownicy: \n" );
        int n = next.nextInt();
        System.out.println("Suma ziarenek Twojego piasku na szachownicy jest rowna:" + szachy(n));
    }
}
