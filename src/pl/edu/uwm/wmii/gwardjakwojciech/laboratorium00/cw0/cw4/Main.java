package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw4;

public class Main{
    public static void main(String[] args){
        RachunekBankowy saver1 = new RachunekBankowy(2000.00);
        RachunekBankowy saver2 = new RachunekBankowy(5000.00);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        System.out.println(String.format("Miesieczne odsetki dla konta saver1: %.2f \n", saver1.obliczMiesieczneOdsetki()));
        System.out.println(String.format("Miesieczne odsetki dla konta saver2: %.2f \n", saver2.obliczMiesieczneOdsetki()));

        saver1.dodajDoSalda();
        saver2.dodajDoSalda();
        System.out.println(String.format("Stan Twojego konta saver1 wynosi: %.2f, saver2: %.2f \n", saver1.stanKonta(), saver2.stanKonta() ));

        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.dodajDoSalda();
        saver2.dodajDoSalda();

        System.out.println(String.format("Miesieczne odsetki dla konta saver1: %.2f \n", saver1.obliczMiesieczneOdsetki()));
        System.out.println(String.format("Miesieczne odsetki dla konta saver2: %.2f \n", saver2.obliczMiesieczneOdsetki()));
        System.out.println(String.format("Stan Twojego konta saver1 wynosi: %.2f, saver2: %.2f \n", saver1.stanKonta(), saver2.stanKonta() ));
    }
}