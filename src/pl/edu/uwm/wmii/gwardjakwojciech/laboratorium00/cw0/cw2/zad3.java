package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw2;
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class zad3 {
    public static void generuj(int[] tab, int n, int max, int min){
        Random r = new Random();
        for(int i=0; i<n; i++){
            tab[i] = r.nextInt(max - min + 1) - max;
        }

    }
    public static void main(String[] args) {
        Scanner wejscie = new Scanner(System.in);
        int m, n, k;
        int[][] a, b, c;
        System.out.println("Podaj ilosc wierszy macierzy A: ");
        m = wejscie.nextInt();
        System.out.println("Podaj ilosc kolumn macierzy A: ");
        n = wejscie.nextInt();
        System.out.println("Podaj ilosc kolumn macierzy B: ");
        k = wejscie.nextInt();
        a = new int[m][n];
        b = new int[n][k];
        c = new int[m][k];
        for(int i = 0; i<m; i++) {
            generuj(a[i], n, 10, 0);
        }
        for(int i = 0; i<n; i++){
           generuj(b[i], k, 10, 0);
        }
        for(int[] row:a){
            System.out.println(Arrays.toString(row));
        }
        for(int[] row:b){
            System.out.println(Arrays.toString(row));
        }
        for(int i = 0; i<m; i++){
            for(int j = 0; j<k; j++){
                for(int l = 0; l<n; l++){
                    c[i][j] += a[i][l] * b[l][j];
                }
            }
        }
        for(int[] row : c){
            System.out.println(Arrays.toString(row));
        }
}
}
