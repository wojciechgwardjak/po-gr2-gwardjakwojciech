package pl.imiajd.gwardjak;

import java.time.LocalDate;

public abstract class OsobaDwa{
    public OsobaDwa(String nazwisko, String imie,  boolean plec, LocalDate dataUrodzenia){
        this.nazwisko = nazwisko;
        this.plec = plec;
        this.imie = imie;
        this.dataUrodzenia = dataUrodzenia;
    }
    public abstract String getOpis();
    public String getNazwisko(){
        return nazwisko;
    }
    public String getImie(){
        return imie;
    }
    public boolean getPlec(){
        return plec;
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }
    private String imie;
    private String nazwisko;
    private boolean plec;
    private LocalDate dataUrodzenia;
}