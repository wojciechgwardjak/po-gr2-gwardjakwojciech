package pl.edu.uwm.wmii.gwardjakwojciech.laboratorium00.cw0.cw9;

import pl.imiajd.gwardjak.StudentTrzy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main (String args[]){
        ArrayList<StudentTrzy> grupa = new ArrayList<StudentTrzy>();
        grupa.add(new StudentTrzy("Kowalski", LocalDate.parse("1990-05-05"), 5.0));
        grupa.add(new StudentTrzy("Kowalski", LocalDate.parse("1994-05-05"), 5.0));
        grupa.add(new StudentTrzy("Nowakowsky", LocalDate.parse("1995-05-05"), 5.0));
        grupa.add(new StudentTrzy("Kolarsky", LocalDate.parse("1995-05-05"), 5.0));
        grupa.add(new StudentTrzy("Wiercipięta", LocalDate.parse("1998-05-05"), 5.0));
        System.out.println(grupa);
        grupa.sort(StudentTrzy::compareTo);
//        Collections.sort(grupa);
        System.out.println(grupa.toString());
    }
}
