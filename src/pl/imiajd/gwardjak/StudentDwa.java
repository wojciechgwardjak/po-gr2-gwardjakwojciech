package pl.imiajd.gwardjak;
import java.time.LocalDate;

public class StudentDwa extends OsobaDwa {
    public StudentDwa(String nazwisko, String imie, boolean plec, LocalDate dataUrodzenia, String kierunek, double sredniaOcen){
        super(nazwisko, imie, plec, dataUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }
    public String getOpis(){
        return "kierunek studiów " + kierunek;
    }
    public double getSredniaOcen(){
        return sredniaOcen;
    }
    public void setSredniaOcen(double nowa){
        sredniaOcen = nowa;
    }
    private double sredniaOcen;
    private String kierunek;
}